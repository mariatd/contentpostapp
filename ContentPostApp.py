import webapp

PAGE = """
<!DOCTYPE html>
<html lang='en'>
  <head>
    <script src='https://unpkg.com/htmx.org@1.9.10'></script>
  </head>
  <body>
    <div>
        <div>
            Current content: <div id=content></div>
        </div>
        <button hx-get='/content/{resource}'
          hx-trigger='click'
          hx-target='#content'
          hx-swap='innerHTML'>
          Get
        </button>
    </div>
    <div>
        <div>
            New content: <input name='content' type='text'/>
        </div>
        <button hx-post='/content/{resource}'
          hx-include="[name='content']"
          hx-trigger='click'
          hx-target='#content'
          hx-swap='innerHTML'>
          Post
        </button>
    </div>
  </body>
</html>
"""

PAGE_NOT_FOUND = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <p>Resource not found: {resource}.</p>
  </body>
</html>
"""

PAGE_NOT_ALLOWED = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <p>Method not allowed: {method}.</p>
  </body>
</html>
"""

class ContentApp(webapp.WebApp):

    def __init__(self, hostname, port):
        self.contents = {}
        super().__init__(hostname, port)

    def parse(self, request):
        data = {}
        parts = request.split(' ', 2)
        data['method'] = parts[0]
        data['resource'] = parts[1]
        if len(parts) == 3:
            data['body'] = parts[2].split('\r\n\r\n', 1)[1] if '\r\n\r\n' in parts[2] else None
        else:
            data['body'] = None
        return data

    def process(self, data):
        method = data['method']
        resource = data['resource']

        if method == 'GET':
            return self.get(resource)
        elif method == 'POST':
            return self.post(resource, data['body'])
        else:
            return "405 Method not allowed", PAGE_NOT_ALLOWED.format(method=method)

    def get(self, resource):
        print("RESOURCE:", resource)
        if resource.startswith('/content/'):
            content_res = resource.split('/', 3)[2]
            if content_res in self.contents:
                return "200 OK", self.contents[content_res]
            else:
                return "404 Resource Not Found", PAGE_NOT_FOUND.format(resource=resource)
        else:
            return "200 OK", PAGE.format(resource=resource[1:])

    def post(self, resource, body):
        if resource.startswith('/content/'):
            content_res = resource.split('/', 3)[2]
            content = body.split('=', 1)[-1]
            self.contents[content_res] = content
            return "200 OK", content
        elif resource in self.contents:
            return "405 Method Not Allowed", PAGE_NOT_ALLOWED.format(method='POST')
        else:
            return "404 Resource Not Found", PAGE_NOT_FOUND.format(resource=resource)


if __name__ == "__main__":
    try:
        webApp = ContentApp ("localhost", 1234)
    except KeyboardInterrupt:
        print("bye!")
